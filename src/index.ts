import { Order } from "./classes/Order";
import { Customer } from "./classes/Customer";
import { Product } from "./classes/Product";
import { Clothing } from "./classes/Clothing";
import { Shoes } from "./classes/Shoe";
import { StandardDelivery } from "./classes/StandardDelivery";
import { ShoeSize, ClothingSize } from "./utils";

//Customer

let chloe = new Customer(2, "Chloé", "chloéberot2@gmail.com")
chloe.adress = {
    city: "Oloron Sainte Marie",
    street: "5 rue Pierre d'Aguere",
    postalCode: "64400",
    country: "France"
}

console.log("demo customer.displyInfo()\n")

//Product

let cacao = new Product(1, "cacao", 10, 10, { length: 10, width: 15, height: 20 })

console.log("demo product.displayInfo()\n")

let chaussures = new Shoes(ShoeSize.EU36, 3, "Super chaussures rouges méga trop cools", 20, 10000, { length: 10, width: 15, height: 20 })
let vetements = new Clothing(ClothingSize.M, 4, "T-shirt bleu trop stylé", 40, 666, { length: 10, width: 15, height: 20 })

console.log(chaussures.displayDetails())
console.log(vetements.displayDetails())


// Order
const command1 = new Order(1, chloe, [cacao], "13/03/2001")
command1.setDelivery(new StandardDelivery)

command1.addProduct(cacao)
command1.removeProduct(cacao.productId)


console.log(`calculateWeight\n ${command1.calculateWeight()}`)
console.log(`calculateTotal\n ${command1.calculateTotal()}`)
console.log(`display order\n ${command1.displayOrder()}`)

console.log(`estimateDeliveryTime\n ${command1.estimateDeliveryTime}`)
console.log(`calculateShippingFee\n ${command1.calculateShippingCosts()}`)