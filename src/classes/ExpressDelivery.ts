import { Deliverable } from "../interfaces/Deliverable"

export class ExpressDelivery implements Deliverable {
    estimateDeliveryTime(weight: number) {
        return (weight < 10 ? 1 : 3)
    }
    calculateShippingFee(weight: number) {
        if (weight < 1) {
            return (3)
        } else if (weight <= 5) {
            return (8)
        } else {
            return (30)
        }
    }
}