import { Product } from "./Product"
import { ClothingSize } from "../utils"
import { Dimensions } from "../utils"
export class Clothing extends Product {
    size: ClothingSize
    constructor(
        size: ClothingSize,
        productId: number,
        name: string,
        weight: number,
        price: number,
        dimensions: Dimensions) {
        super(productId, name, weight, price, dimensions)
        this.size = size
    }
    displayDetails(): string {
        return (` size ${this.size}\n clothID: ${this.productId},\n name: ${this.name},\n weight: ${this.weight},\n price: ${this.price},\n dimensions: -largeur: ${this.dimensions.width} -longueur: ${this.dimensions.length} -hauteur: ${this.dimensions.length}
        `)
    }
}