import { Product } from "./Product"
import { ShoeSize } from "../utils"
import { Dimensions } from "../utils"

export class Shoes extends Product {
    size: ShoeSize
    constructor(
        size: ShoeSize,
        productId: number,
        name: string,
        weight: number,
        price: number,
        dimensions: Dimensions) {
        super(productId, name, weight, price, dimensions)
        this.size = size
    }
    displayDetails(): string {
        return (` size: ${this.size}\n shoesID: ${this.productId},\n name: ${this.name},\n weight: ${this.weight},\n price: ${this.price},\n dimensions: -largeur: ${this.dimensions.width} -longueur: ${this.dimensions.length} -hauteur: ${this.dimensions.length}
        `)
    }
}