import { Customer } from "./Customer";
import { Product } from "./Product";
import { Deliverable } from "../interfaces/Deliverable";

export class Order {
    orderId: number;
    customer: Customer;
    productsList: Product[];
    orderDate: string
    delivery: Deliverable | undefined

    constructor(orderId: number, customer: Customer, productsList: Product[], orederDate: string) {
        this.orderId = orderId;
        this.customer = customer;
        this.productsList = productsList;
        this.orderDate = orederDate;
    }
    addProduct(product: Product): void {
        this.productsList.push(product)
    }
    removeProduct(productId: number): void {
        this.productsList = this.productsList.filter(e => e.productId === productId)
    }
    calculateWeight(): number {
        return (this.productsList.reduce((totalWeight, product) => totalWeight + product.weight, 0))
    }
    calculateTotal(): string {
        return (`total price${this.productsList.reduce((totalPrice, product) => totalPrice + product.weight, 0)}`)
    }
    displayOrder(): string {
        return (`
        customer: ${this.customer.displayInfo}\n
        product list: ${this.productsList.map((product: Product) => { return product.displayDetails() })}\n
        total price: ${this.calculateTotal()}
        `)
    }
    setDelivery(delivery: Deliverable): void {
        this.delivery = delivery
    }
    calculateShippingCosts():number {
        return(this.delivery ? this.delivery?.calculateShippingFee(this.calculateWeight()) : 0)
    }
    estimateDeliveryTime(): number {
        return(this.delivery ? this.delivery?.estimateDeliveryTime(this.calculateWeight()) : 0)
    }
}