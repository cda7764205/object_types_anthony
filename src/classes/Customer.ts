import { Adress } from "../utils";

export class Customer {
    customerId: number;
    name: string;
    email: string;
    adress: Adress | undefined;
    constructor(customerId: number, name: string, email: string) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
    };
    displayInfo(): string {
        return (`
        name: ${this.name}\n
        customerId: ${this.customerId}\n
        adresse: ${this.displayAddress()}`)
    }
    displayAddress(): string {
        return (this.adress ? `city${this.adress?.city}\n postalCode${this.adress?.postalCode}\n country${this.adress?.country}` : `Pas d'adresse enregistré`)
    }
}