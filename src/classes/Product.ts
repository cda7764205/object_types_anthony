import { Dimensions } from "../utils";

export class Product {
    productId: number;
    name: string;
    weight: number;
    price: number;
    dimensions: Dimensions;
    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions
    }
    displayDetails(): string {
        return (`
        productiId: ${this.productId}\n
        name: ${this.name}\n
        weight: ${this.weight}\n 
        price: ${this.price}\n
        dimensions: -largeur: ${this.dimensions.width} -longueur: ${this.dimensions.length} -hauteur: ${this.dimensions.length}`)
    }
}